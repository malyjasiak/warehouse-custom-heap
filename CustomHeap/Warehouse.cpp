﻿#include "Warehouse.h"
#include <algorithm>
#include <string>
//Metoda zwracająca listę towarów dostępnych w magazynie wraz z informacją o nich
vector<Merchandise*> * Warehouse::getMerchandise()
{
	//w pierwszym wierszu w pliku znajduje się informacja o tym, ile towarów się w nim znajduje. Przyda się to w pętli wczytującej dane
	int size;
	//Tworzymy nowy wektor, który będzie przechowywał wszystkie towary i zostanie zwrócony
	vector<Merchandise *> * merchandise = new vector<Merchandise*>();
	//Deklaracja zmiennych, które definiują towar
	string name, location;
	long id;
	int count;
	//Otworzenie pliku do odczytu
	ifstream fin;
	fin.open(PATH_TO_FILE, ios::in);
	if(!fin.is_open())
	{
		cout << "Nie udalo sie otworzyc pliku!" << endl;
		return merchandise;
	}
	fin >> size;
	//W pętli wczytujejy kolejne rekordy z pliku. Ograniczeniem jest <size> wczytany wcześniej
	for (auto i = 0; i < size; i++)
	{
		fin >> name;
		fin >> id;
		fin >> location;
		fin >> count;
		//na podstawie pobranych danych tworzymy nowy towar i dodajemy do wektora
		merchandise->push_back(new Merchandise(name, id, location, count));
	}
	//zamknięcie strumienia do wczytytywania z pliku	
	fin.close();
	return merchandise;
}
//Metoda zapisująca listę towarów spowrotem do pliku
void Warehouse::saveMerch(vector<Merchandise*> * merchandise)
{
	//Otworzenie strumienia do zapisu do pliku
	ofstream fout;
	fout.open(PATH_TO_FILE, ios::out);
	if (!fout.is_open())
	{
		cout << "Nie udalo sie otworzyc pliku!" << endl;
		return ;
	}
	//w pierwszym wierszu zapisujemy ilość towarów
	fout << merchandise->size() << endl;
	//W następnych wierszach zapisujemy dane definiujące każdy z towarów
	for (auto merch : *merchandise)
	{
		fout << merch->getName() << FIELD_SEPARATOR;
		fout << merch->getId() << FIELD_SEPARATOR;
		fout << merch->getLocation() << FIELD_SEPARATOR;
		fout << merch->getCount() << RECORD_SEPARATOR;
	}
	//Zamknięcie strumienia
	fout.close();
}

const vector<Order*> * Warehouse::getOrders() const
{
	return orders.getOrders();
}
//Metoda przeszukująca listę towarów w poszukiwaniu towaru o zadanym id
Merchandise* Warehouse::findMerch(vector<Merchandise *> * merchandise, long id)
{
	//Iterujemy po wektorze towarów
	for (Merchandise * merch : *merchandise)
	{
		//Jeśli znaleziono towar o takim samym id, to zwracamy wskaźnik na niego
		if(merch->getId() == id)
		{
			return merch;
		}
	}
	//Jeśli nie udało się znaleźć przedmiotu, zwracamy nullptr
	return nullptr;
}

//Metoda realizująca złożenie nowego zamówienia
void Warehouse::placeOrder(Order * order)
{
	//Dodanie nowej oferty ogranicza się do wrzucenia jej do kopca, który załatwia dalsze sortowanie
	orders.push(order);
}


//Metoda realizująca dostawę
void Warehouse::supply()
{
	//Najpierw pobieramy z pliku listę wszystkich towarów
	auto merchandise = getMerchandise();
	//Następnie iterujemy po wektorze towarów
	for(auto merch : *merchandise)
	{
		//Dla każdego towaru losujemy liczbę z przedziału <0, 1) i jeśli jest mniejsza od stałej
		//SUPPLY_PROBABILITY, to dokładamy również losową ilość sztuk(niewiększą, niż MAX_SUPPLY_AMOUNT)
		if(rand() % 100 < SUPPLY_PROBABILITY_PERCENTAGE)
		{
			int supplyAmount = rand() % MAX_SUPPLY_AMOUNT + 1;
			merch->supply(supplyAmount);
			cout << "Uzupelniono zapas towaru [" << merch->getId() << "] " << merch->getName() << " o " << supplyAmount << " sztuk" << endl;
		}
	}
	//Po uzupełnieniu towarów zapisujemy dane do pliku
	saveMerch(merchandise);

	if (merchandise != nullptr)
	{
		delete merchandise;
	}
}

//Metoda podmieniająca kopiec przechowujący zamówienia na inny
//Przyda się przy okazji realizowania zamówień
void Warehouse::swapOrders(const OrdersMinHeap& orders)
{
	this->orders.replaceHeap(orders);
}

//Metoda realizująca wszytkie zamówienia
void Warehouse::handleAllOrders()
{
	//Tworzymy nowy kopiec, w którym będziemy przechowywali niezrealizowane zamówienia
	OrdersMinHeap notHandledOrders;
	//Pobieramy listę towarów
	auto merchandise = getMerchandise();
	Order * order;
	
	//Pobieramy z kopca wierzchołek, dokpóki kopiec zawieta jakiekolwiek elementy
	//Parafrazując, realizujemy ofertu według proprytetu.
	while((order = orders.top()) != nullptr)
	{
		//Próbujemy zrealizować zamówienie i wynik przechowujemy w zmiennej bool
		bool handlingSucceded = handleOrder(order, merchandise);
		if(!handlingSucceded)
		{
			//Jeśli nie udało się zrealizować ofery, to wrzucamy ją do specjalnego kopca, zeby nie zajmowała miejsca w 
			//kopcu z realizowanymi zamówieniami
			notHandledOrders.push(new Order(*order));
		}
		//Ściągamy wierzchołek z kopca
		orders.pop();
	}
	//Po zrealizowaniiu zamówień:
	//	Zapisujemy towaty do pliku
	saveMerch(merchandise);
	//	Podmieniamy kopiec z zamówieniami na kopiec z niezrealizowanymi zamówieniami, żeby spróbować je zrealizować później
	swapOrders(notHandledOrders);
	if (merchandise != nullptr)
	{
		delete merchandise;
	}
}
//Metoda realizująca pojedyncze zamówienie
bool Warehouse::handleOrder(Order * order, vector<Merchandise *> * merchandise)
{
	//Założenie jest takie, że zamówienie albo realizujemy w całości, albo wcale, dlatego najpierw sprawdzamy, czy da się zrealizować zamówienie na wszystkie towary, a dopiero potem realizujemy
	bool canHandle = true;
	//Wiadomosc o przemdiotach, ktore byly niedostepne
	ostringstream buffer;
	//Iterujemy po towarach, które są na zamówieniu i sprawdzamy, czy da się je uzyskać, czyli czy towar istnieje, i czy jest wystarczająco wiele sztuk
	for(auto item : order->getOrderItems())
	{
		//Szukamy towaru, na który jest zamówienie, żeby znać jego stan na magazynie
		auto merch = findMerch(merchandise, item->getMerchandiseId());
		if(merch == nullptr || merch->getCount() < item->getQuantity())
		{
			//Jeśli nie udało się zrealizować, to ustawiamy <canHandle> na false		
			buffer << "\tBrak towaru o id " << item->getMerchandiseId() << "\n";
			canHandle = false;
		}
	}
	//Kończymy, jeśli nie udało się zrealizować zamówienia na chociażby jeden towar
	if(!canHandle)
	{
		cout << "Zamowienie o id " << order->getId() << " i priorytecie " << order->getPriority() << " nie moze zostac zrealizowane. Szczegoly:" << endl;
		cout << buffer.str() << endl;
		return false;
	}

	//realizacja zamowienia
	cout << "Zrealizowano zamowienie o id " << order->getId() << " i priorytecie " << order->getPriority() <<". Szczegoly: "<< endl;
	//Iterujemy po towarach na zamówieniu raz jeszcze
	for (auto item : order->getOrderItems())
	{
		//Szukamy towaru, żeby móc wykonać na nim operację zmniejszenia dostępnej ilości
		auto merch = findMerch(merchandise, item->getMerchandiseId());
		//Zmniejszenie ilości dostępnych sztuk towaru o ilość z zamówienia
		merch->retrieve(item->getQuantity());
		cout << "\tWydano przedmiot o id " << item->getMerchandiseId() << " w ilosci " << item->getQuantity() << " sztuk" << endl;
	}
	return true;
}

void Warehouse::printOrders() const
{
	cout << "<<----------Lista zamowien---------->>" << endl;
	for(auto order : *getOrders())
	{
		order->print();
		cout << endl;
	}
	cout << ">>----------------------------------<<" << endl;
}

void Warehouse::printMerchandise()
{
	cout << "<<----------Lista towarow---------->>" << endl;
	auto merchandise = getMerchandise();
	for (auto merch : *merchandise)
	{
		merch->print();
		cout << endl;
	}
	if(merchandise != nullptr)
	{
		delete merchandise;
	}
	cout << ">>----------------------------------<<" << endl;
}