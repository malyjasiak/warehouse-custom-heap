﻿#pragma once
#include "OrderItem.h"
#include <list>
#include<iostream>
#include<string>
using namespace std;

/*Klasa odpowiadająca zamówieniu. 
 *	priority - priorytet zamówienia. Im niższy, tym zamówienie zostanie rozpatrzone szybciej. Minimalny priorytet to 0
 *	orderItems - lista przemiotów, których dotyczy zamówienie. Na jej podstawie zostanie zrealizowane zamówienie, a odpowiednie wartości odjęte od stanu magazynowego
 */

class Order
{
private:
	long id;
	int priority;
	list<OrderItem*> orderItems;

public:
	//Konstruktor zabezpieczony przed dodaniem ujemnego priorytetu
	explicit Order(long id, int priority) : id(id), priority(priority <= 0 ? 0 : priority) {  }
	Order(const Order&);
	~Order();
	//Akcesory
	long getId() const;
	void setId(long id);
	int getPriority() const;
	void setPriority(int priority);
	list<OrderItem*> getOrderItems() const;
	
	//Metoda sprawdzająca, czy zamówienie dotyczy danego identyfikatora produktu
	OrderItem * hasMerchId(long);

	//Metoda dodająca nowy przedmiot do zamówienia
	void addOrderItem(long, int);
	
	//Operator porównania dla dwóch obiektów tego typu. Porównywane sa priorytety
	bool operator<(const Order&) const;

	//Metoda wykorzystywana w przeładowanym operatorze <. Porównuje priorytety
	int compareTo(const Order *) const;
	void print();
};
