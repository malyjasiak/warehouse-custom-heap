﻿#pragma once
#include <complex>

//funkcja zwracająca liczbę węzłów w kopcu aż do zadanego poziomu (włącznie)
static int nodesCountAtLevel(int level)
{
	return level < 0 ? 0 : std::pow(2, level) + nodesCountAtLevel(level - 1);
}