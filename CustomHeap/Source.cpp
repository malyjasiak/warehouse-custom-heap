#pragma once
#include<iostream>
#include<ctime>
#include<string>
#include "Warehouse.h"
using namespace std;

void mainMenu();
void placeOrderMenu();
void handleOrdersMenu();
void supplyMenu();
void stateMenu();

const static string WELCOME_MESSAGE = "------Witaj w symulatorze magazynu------";

const static string MAIN_MENU_TEXT =
	"\n1. Dodaj zamowienie\n2. Obsluz zamowienia\n3. Dostawa\n4. Stan\n5. Wyjscie";

const static string STATE_MENU_TEXT =
	"\n1. Wyswietl liste zamowien\n2. Wyswietl stan magazynu\n3. Wroc";

Warehouse warehouse;

int main() {
	srand(time(nullptr));
	cout << WELCOME_MESSAGE << endl;
	mainMenu();
	return 1;
}


void mainMenu()
{
	int mainMenuOption;
	bool mainMenuExit = false;
	while(!mainMenuExit)
	{
		cout << MAIN_MENU_TEXT << endl;
		cout << "Wybor: ";
		cin >> mainMenuOption;
		switch(mainMenuOption)
		{		
		case 1:
			placeOrderMenu();
			break;
		case 2:
			handleOrdersMenu();
			break;
		case 3:
			supplyMenu();
			break;
		case 4:
			stateMenu();
			break;
		case 5:
			mainMenuExit = true;
			break;
		default:
			cout << "Nieprawidlowy wybor" << endl;
			break;
		}
	}
}

void placeOrderMenu()
{
	int priority;
	cout << "Podaj priorytet: ";
	cin >> priority;
	if(priority < 0)
	{
		cout << "Podano nieprawidlowy priorytet" << endl;
		return;
	}
	long id;
	cout << "Podaj id zamowienia: ";
	cin >> id;
	
	Order * order = new Order(id, priority);
	string choice = "";
	long merchId;
	int merchQuantity;
	cout << "--Dodawanie towarow do zamowienia--" << endl;
	while(choice != "n")
	{
		choice = "";
		cout << "\tPodaj id towaru: ";
		cin >> merchId;
		cout << "\tPodaj ilosc towaru: ";
		cin >> merchQuantity;
		order->addOrderItem(merchId, merchQuantity);
	
		while (choice != "n" && choice != "y")
		{
			cout << "\tCzy chcesz dodac jescze jeden towar? (y/n) ";
			cin >> choice;
		}
	}
	warehouse.placeOrder(order);
}

void handleOrdersMenu()
{
	warehouse.handleAllOrders();
}

void supplyMenu()
{
	warehouse.supply();
}

void stateMenu()
{
	int menuOption;
	bool menuExit = false;
	while (!menuExit)
	{
		cout << STATE_MENU_TEXT << endl;
		cout << "Wybor: ";
		cin >> menuOption;
		switch (menuOption)
		{
		case 1:
			warehouse.printOrders();
			break;
		case 2:
			warehouse.printMerchandise();
			break;
		case 3:
			menuExit = true;
			break;
		default:
			cout << "Nieprawidlowy wybor" << endl;
			break;
		}
	}
}
