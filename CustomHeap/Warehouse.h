﻿#pragma once
#include <string>
#include <list>
#include "Order.h"
#include "Merchandise.h"
#include <vector>
#include "OrdersMinHeap.h"
using namespace std;

//Ścieżka do pliku z danymi
const static string PATH_TO_FILE = "./towary.txt";

//Znak rozdzielający atrybuty obiektu po zapisaniu do pliku
const static string FIELD_SEPARATOR =  " ";

//Znak rozdzielający kolejne obiekty po zapisaniu do pliku
const static string RECORD_SEPARATOR =  "\n";

//Prawdopodobieństwo dostawy dla towaru w procentach
const static int SUPPLY_PROBABILITY_PERCENTAGE = 60;

//Maksymalna ilość towaru, która może być dodana podczas dostawy
const static int MAX_SUPPLY_AMOUNT = 20;

/*
 * Warehouse to magazyn
 * 
 * orders - kopiec przechowujący oczekujące zamówienia. Struktura to kopiec, im niższa wartość priorytetu dla zamówienia, tym szybciej będzie rozpatrzone
 */
class Warehouse
{
private:
	OrdersMinHeap orders;

public:
	//Metoda zwracająca listę towarów dostępnych w magazynie wraz z informacją o nich
	static vector<Merchandise *> * getMerchandise();

	//Metoda przeszukująca listę towarów w poszukiwaniu towaru o zadanym id
	static Merchandise * findMerch(vector<Merchandise *> *, long);

	//Metoda zapisująca listę towarów spowrotem do pliku
	static void saveMerch(vector<Merchandise *> *);
	
	//Metoda zwracająca wektor zamówień w takiej kolejności, w jakiej są w kopcu 
	const vector<Order *> * getOrders() const;

	//Metoda realizująca złożenie nowego zamówienia
	void placeOrder(Order *);

	//Metoda realizująca dostawę
	static void supply();

	//Metoda podmieniająca kopiec przechowujący zamówienia na inny
	void swapOrders(const OrdersMinHeap& orders_min_heap);

	//Metoda realizująca wszytkie zamówienia
	void handleAllOrders();

	//Metoda realizująca pojedyncze zamówienie
	static bool handleOrder(Order *, vector<Merchandise *> *);

	//Metoda wyświetlająca wszystkie zamówienia
	void printOrders() const;

	//Metoda wyświetlająca wszystkie towary w magazynie
	static void printMerchandise();
};
