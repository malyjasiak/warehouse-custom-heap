﻿#pragma once
#include<iostream>
#include<cmath>
#include<fstream>
#include<string>
#include "Utils.h"
#include "Order.h"
#include <vector>

using namespace std;

/*
 * MIN-HEAP zaimplementowany do przechowywania zamówień
 * 
 * orders - wektor zamówień, który przechowuje je odpowiednio posortowane. Wektor zamiast listy z uwagi na możliwość prostego odwołania się do konkretnego indeksu w strukturze.
 * 
 * komentarze dla poszczególnych metod znajdują się w OrdersMinHeap.cpp
 */
class OrdersMinHeap
{
private:
	vector<Order *> orders;
public:
    OrdersMinHeap();
	OrdersMinHeap(const OrdersMinHeap& heap);
    ~OrdersMinHeap();

	static int parent(int child);
	static int left(int parent);
	static int right(int parent);

	const vector<Order *> * getOrders() const;
	void replaceHeap(const OrdersMinHeap&);
	Order * top() const;
    void push(Order * order);
    bool pop();
    void heapifyUp(int);
    void heapifyDown(int);
    void swap(int, int);
    bool isEmpty() const;
    void print() const;
    void printLevels();
};