﻿#pragma once
#include<string>
using namespace std;
//Klasa Merchandise odpowiada pojedynczemu towarowi znajdującemu się w magazynie. Atrybuty odpowiadają tym, które zostały wyspecyfikowane w poleceniu.
class Merchandise
{
private:
	string name;
	long id;
	string location;
	int count;

public:
	Merchandise(const string& name, long id, const string& location, int count = 0)
		: name(name),
		id(id),
		location(location),
		count(count)
	{}

	// Akcesory
	string getName() const { return name; }
	void setName(string name) { this->name = name; }
	long getId() const { return id; }
	void setId(long id) { this->id = id; }
	string getLocation() const { return location; }
	void setLocation(string location) { this->location = location; }
	int getCount() const { return count; }
	void setCount(int count) { this->count = count; }

	//Metoda pobieraca z magazynu okreslona ilosc sztuk danego towaru - służy do realizacji zamówienia
	bool retrieve(int quantity);

	//Metoda uzupełniająca ilość sztuk o zadaną ilość. Ilosc sztuk nie może przekroczyć 100. Służy do realizacji dostawy
	void supply(int quantity);

	//Metoda wypisująca na standardowe wyjście wszystkie dane dotyczące danego towaru
	void print() const;
};