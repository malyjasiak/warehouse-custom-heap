﻿#pragma once

#include "OrdersMinHeap.h"

OrdersMinHeap::OrdersMinHeap()
{
}

OrdersMinHeap::OrdersMinHeap(const OrdersMinHeap& that)
{
	this->orders.clear();
	auto thatOrders = that.getOrders();
	for (Order * order : *thatOrders)
	{
		this->orders.push_back(new Order(*order));
	}

}

OrdersMinHeap::~OrdersMinHeap() 
{
	for(auto order : orders)
	{
		if(order != nullptr)
		{
			delete order;
		}
	}
}

const vector<Order*> * OrdersMinHeap::getOrders() const
{
	return &orders;
}

void OrdersMinHeap::replaceHeap(const OrdersMinHeap& orders)
{
	this->orders.clear();
	for(Order * order : *orders.getOrders())
	{
		this->orders.push_back(new Order(*order));
	}
}

//Metoda obliczająca indeks lewego potomka dla węzła <parent>
int OrdersMinHeap::left(int parent)
{
    return parent * 2 + 1;
}

//Metoda obliczająca indeks prawego potomka dla węzła <parent>
int OrdersMinHeap::right(int parent) {
    return parent * 2 + 2;
}

//Metoda obliczająca indeks rodzica dla danego potomka <child>
int OrdersMinHeap::parent(int child)
{
	return int(floor((child - 1) / 2));
}

//Metoda zwracająca element będący na szczyci kopca
Order * OrdersMinHeap::top() const
{
	return isEmpty() ? nullptr : orders[0];
}	

//Metoda wrzucająca do kopca nowe zamówienie
void OrdersMinHeap::push(Order * order)
{
	//Nowe zamówienie jest umieszczane na końcu wektora
	orders.push_back(order);
	//Następnie uruchamiana jest operacja kopcowania w górę, aby umieścić nowe zamówienie na odpowiedmin miejscu (zachowanie struktury kopca)
	heapifyUp(orders.size() - 1);
}

//Metoda ściągająca (usuwająca) z węzeł z wierzchołka
bool OrdersMinHeap::pop()
{
	//Jeśli kopiec jest pusty, to kończymy zwracając false
    if (isEmpty())
    {
        return false;
    }
	//Teraz zamieniamy miejscami pierwszy i ostatni węzeł, żeby można było go spokojnie usunąć
	auto first = 0;
	auto last = orders.size() - 1;
	swap(first, last);

	//Zapisujemy wskaźnik do ostatniego elementy, żeby móc go usunąć
	auto lastObject = orders.back();
	orders.pop_back();
	delete lastObject;

	//Teraz kolej na operację kopcowania w dół od indeksu 0, na którym znajduje się aktualnie ostatni element z wektora. Cel to przywrócenie prawidłowej struktury
    heapifyDown(0);
    return true;
}

void OrdersMinHeap::print() const
{
    cout << "Aktualna ilosc elementow: " << orders.size() <<endl;
    for (auto order : orders)
    {
		order->print();
    }
}
//Metoda drukująca wartości priorytetów z podziałem na poziomy kopca. Przydatne do debugowania
void OrdersMinHeap::printLevels() {
    
    cout << "Aktualna ilosc elementow: " << orders.size() <<endl;

    if(isEmpty()){
        return;
    }
	printf("\nPoziom [0]:");
    int level = 0;
	int i = 0;
	for(auto order : orders)
	{
		printf(" %i", order->getPriority());
		if (i + 1 >= nodesCountAtLevel(level))
		{
			printf("\nPoziom [%i]:", ++level);
		}   
		i++;
    }
	cout << endl;
}

//Kopcowanie w górę
void OrdersMinHeap::heapifyUp(int node)
{
	//Wykonywane rekurencyjnie dopóki nie dojdziemy do najwyższego węzła, czyli szczytu kopca mającego w wektorze indeks 0
    if (node != 0)
    {
		//Obliczamy index rodzica
        int parentIndex = parent(node);
		//Porównujemy priorytet w rodzicu oraz potomku i jeśli nie rujnują właściwości kopca, to zostawiamy. 
    	//W przeciwnym razie zamieniamy je miescami i wywołujemt funkcję raz jeszcze od indeksu odpowiadającego zamienionemu potomkowi :D
		if(orders[node]->getPriority() < orders[parentIndex]->getPriority())
		{
			this->swap(node, parentIndex);
			heapifyUp(parentIndex);
		}
    }
}

//Kopcowanie w dół
void OrdersMinHeap::heapifyDown(int node)
{
	//Obliczamy indeksy potomków
    int leftChild = left(node);
    int rightChild = right(node);
	//Sprawdzamy, czy nie znajdują się poza wektorem (oznaczałoby to, że dany węzeł nie ma jednego, bądź dwóch, potomków)
    bool hasleft = leftChild < orders.size();
    bool hasright = rightChild < orders.size();

	//Jeśli nie ma potomków, to oznacza, że jesteśmy na samym dole i kończymy
    if (!hasleft && !hasright)
    {
        return;
    }

    //Jeśli wsześniejszy warunek nie został spełniony, to oznacza, ze coś należy zamienić.
	//Sprawdzamy, czy prawy istnieje i jest niewiększy od lewego
    int smaller;
	if (hasright && (orders[rightChild]->getPriority() < orders[leftChild]->getPriority()))
	{
		//Jeśli tak, to oznaczamy prawy jako mniejszy
        smaller = rightChild;
    }
    else
    {
		//W przeciwnym razie mniejszym będzie lewy
        smaller = leftChild;
    }

	if (orders[smaller]->getPriority() >= orders[node]->getPriority())
	{
		return;
	}
	//Następnie zamieniamy miejscami dany węzeł i potomka oznaczonego jako mniejszy
    swap(node, smaller);

    //Następnie dla zamienionego potomka uruchamiamy kopcowanie w dół
    heapifyDown(smaller);
}

bool OrdersMinHeap::isEmpty() const
{
    return orders.size() == 0;
}

//zamiana dwóch węzłów
void OrdersMinHeap::swap(int src, int dest) {
    auto temp = orders[dest];
    orders[dest] = orders[src];
    orders[src] = temp;
}

