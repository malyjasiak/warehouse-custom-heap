﻿#include"Merchandise.h"
#include <iostream>

//Metoda zmiejszająca ilość sztuk danego towaru o zadaną ilość
bool Merchandise::retrieve(int quantity)
{
	//Jeśli żądana ilość jest większa, niż dostępna, to operacja zostaje przerwana
	if (quantity > count)
	{
		return false;
	}
	//Zmniejszenie ilości dostępnych sztuk o zadaną ilość
	count -= quantity;
	return true;
}

//Metoda dodająca zadaną ilość sztuk danego przedmiotu
void Merchandise::supply(int quantity)
{
	int newCount = count + quantity;	
	//Jeśli nowa ilość przekracza 100, to ilość zatrzyma się na 100. W przeciwnym razie zostanie wpisana nowa ilość
	count = newCount > 100 ? 100 : newCount;
}

void Merchandise::print() const
{
	cout << "ID: " << this->getId() << endl;
	cout << "Nazwa: " << this->getName() << endl;
	cout << "Lokalizacja: " << this->getLocation() << endl;
	cout << "Ilosc na magazynie: " << this->getCount() << endl;
}
