﻿#pragma once

/*
 *Reprezentacja zamówienia na konkretny towar
 *Główne amówienie może posiadać wiele zamówień na konretny towar
 *
 *mechanfiseId - identyfikator towaru
 *quantity - ilość sztuk danego towaru, na które jest zamówienie
*/
class OrderItem
{
private:
	long merchandiseId;
	int quantity;

public:
	OrderItem(long merchandiseId, int quantity)
		: merchandiseId(merchandiseId),
		  quantity(quantity)
	{
	}

	//Akcesory
	long getMerchandiseId() const;
	void setMerchandiseId(long);
	int getQuantity() const;
	void setQuantity(int quantity);

	void print() const;
	//Metoda zwiększająca ilość sztuk, na które jest zamówienie
	//WYkorzystywana w przypadku, jeśli próbujemy dodać do zamówienia towar, który już się na nim znajduje
	void increaseQuantity(int);
};


