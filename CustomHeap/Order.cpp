﻿#include"Order.h"

//Konstruktor kopiujący. Przepisuje listę przedmiotów i kopiuje pozstałe atrybuty
Order::Order(const Order& that)
{
	this->id = that.getId();
	this->priority = that.getPriority();
	this->orderItems.clear();
	for (OrderItem * item : that.getOrderItems())
	{
		this->addOrderItem(item->getMerchandiseId(), item->getQuantity());
	}
}

Order::~Order()
{
	for (OrderItem* item : orderItems)
	{
		if(item != nullptr)
		{
			delete item;
		}
	}
}

long Order::getId() const
{
	return id;
}

void Order::setId(long id)
{
	this->id = id;
}

int Order::getPriority() const
{
	return priority;
}

void Order::setPriority(int priority)
{
	this->priority = priority;
}

//Wyszukiwanie identyfikatora towaru na zamówieniu 
OrderItem * Order::hasMerchId(long id)
{
	for (auto item : orderItems)
	{
		//Jeśli udało się znaleźć przedmiot o takim samym id, to zwracamy wskaźnik na niego.
		if(item->getMerchandiseId() == id)
		{
			return item;
		}
	}
	//jeśli nie udało się znaleźć, to nullptr
	return nullptr;
}


//DOdawanie nowego przemdiotu do zamówienia
void Order::addOrderItem(long merchId, int quantity)
{
	//Wyszukanie, czy na zamówieniu jest już dany przedmiot
	auto item = hasMerchId(merchId);
	if(item != nullptr)
	{
		//jeśli jest, to zamiast dodawać go ponownie, po prostu zwiększamy ilość
		item->increaseQuantity(quantity);
	}
	else
	{
		//Jeśli natomiast produktu nie ma jeszcze na liście, to zostaje on do niej dodany
		orderItems.push_back(new OrderItem(merchId, quantity));
	}
}

list<OrderItem*> Order::getOrderItems() const
{
	return orderItems;
}

bool Order::operator<(const Order& that) const
{
	//przeładowanie operatora < wykorzystujący porównanie priorytetów
	return this->compareTo(&that) < 0;
}

int Order::compareTo(const Order * that) const
{
	//Porównanie dwóch priorytetów na zasadzie odjęcia jednego od drugiego
	//zwraca róznicę
	return this->getPriority() - that -> getPriority();
}

void Order::print()
{
	cout << "ID: " << id << endl;
	cout << "Priorytet: " << priority << endl;
	cout << "Ilosc przedmiotow: " << orderItems.size() << endl;
	for(auto item : orderItems)
	{
		item->print();
	}
}
